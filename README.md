# Mapas Conceptuales
## La consultoria
```plantuml
@startmindmap
title La consultoria 
* La consultoria \nUna profesión \nde éxito presente \ny futuro
** ¿Qué es la consultoria?
*** ¿Cómo nace?
**** Nace por que existen grandes organizaciones \nque internamente tiene mucha complejidad \ny requieren de sistemas de información \nque den soporte a esos procesos complejos
***** La tencion continua en el mercado \nque obliga a las empresas a que evolucionen \nLas empresas que no evolucionen \neventualmente desaparecen
****** Estan en una constante \nrenovacion interna de procesos \nobligados por la competencia \ny al mismo tiempo estar operando \nel negocio principal
******* Habitualemnte no tienen la capacidad \ninterna para hacer las dos cosas a la vez
******** Por: \nFalta de personal cualificado \nFalta de conocimiento especifico \nFalta de tiempo
********* Recurren a empresas que les ayudan
********** Transformarse \n Operar el negocio actual
*** Caracteristicas
**** Conocimiento sectorial \nConocimiento tecnico \nCapacidad de plantear soluciones
** ¿Qué tipos de servicios presta?
*** Consultoria
**** Reingenieria de procesos
***** Ayudar a las empresas a que organicen sus actividades siguiendo \nun determinado esquema de funcionamiento
**** Gobierno TI
***** Consultoria especificamente dirigida al CEO
****** Calidad \nMetodologias de desarrollo \nArquitectura empresarial
**** Oficina de proyectos
***** Proyectos complejos con muchos intervinientes Involucran un area IT
****** La gestión de los proyectos es realizada sin una metodologia \nadecuada y orientada a conseguir el exito
**** Analitica avanzada de datos
***** La informacion es crucial para las empresas
****** Disponer en tiempo real de la informacion \nque les permita tomar desciciones
*** Integracion
**** Desarrollos a medida
***** De diferentes tipos de lenguajes y tecnologias
**** Aseguramiento de la calidad de Software
***** Procesos de negocio de extremo a extremo e intervienen en su solucion diferentes \nsistemas y asegurar que cuando se vea afectado 
****** Se realiza su implantación de manera adecuada y con la calidad necesaria de extremo a extremo \nsuele requerir de poner proyectos especificamente dirigidos a gestionarlos
**** Infrestucturas 
***** Uso de nuevas tecnologias \nUso de las plataformas en la nube
**** Soluciones de mercado
***** Las empresas suelen desarrollar sus sitemas de soporte a su operacion \ny a su negocio basandose en software preconstruido y parametrizable
****** Requieren conocimiento profundo de la solucion para entendiendo \nla problematica trasladar esa parametrización a las soluciones preconstruidas
******* Ofreciendo una solución viable 
*** Externalizacion 
**** Gestión de aplicaciones
***** Las empresas delegan todo el mantenimiento y \nevolución de sus sistemas a un tercero
**** Servicios SQA 
***** Pruebas y testing de aplicaciones
**** Operación y administración de infrestructuras
**** Proceso de negocio
***** Area de crecimiento importante \nProcesos completos de negocio
*** SMACT
**** Social \nMobility \nAnalytics \nCloud \nTraditional
*** Servicios de Estrategia
**** Definicion Modelos Organizativos y Operativos \nDefinicion Planes/Modelos de Negocio \nDefinicion de Estrategia Corporativa, Comercial/Tecnologica \nSegmentacion de Mercado \nDefinicion de Productos y servicios \nDefinicion de Planes Operativos por areas \nPlanes de Medios \nSegmentacion de Clientes \nFusiones, Adquisicioes y Alianzas \nGestion de Campañas
** ¿Porqué es una profesion de futuro?
*** En la sociedad del conocimiento la consultoria, es un modo de vida \nque permite alcanzar la felicidad profesional
**** Felicidad Profecional
***** Retribucion competitiva \n+ \nCarrera profesional \n+ \nReto
*** Distribucion de ingresos
**** Por servicios
***** Outsorcing \n46.0% \nDesarollo e integracion \n37.8% \nConsultoria \n16.2%
**** Principales Industrias
***** Servicios financieros \n29.0% \nAdministraciones publicas 15.8% \nTelecominicaciones 13.2%
*** Empleo
**** 144,265 profesionales en 2015 \nEl empleo aumenta \n3.4% respecto a 2014
*** Tendencias
**** Transformacion Digital \nIndustria conectada 4.0 \nSmart cities
** ¿Qué exige la consultoria?
*** Aptitud
**** Buenos profesionales
*****_ Por su:
****** Formacion y experiencia \nCapacidad de trabajo y evolucion
*****_ Capaces de:
****** Colaborar en un proyecto comun \nTrabajar en equipo \nOfrecer honestidad y sinceridad
**** Evolucion natural
***** Gestor de Tecnica
****** Gestor de personas
*** Actitud
**** Proactividad
***** Implica iniciativa y anticipacion \nEs impresindible en consultoria
**** Voluntad de mejora
***** Actitud activa en construccion de empresa \nAportacion de ideas de mejora
**** Responsabilidad
***** Asumir trabajos para hacerlos \nNo devolver nuevos problemas \nNo esgrimir razones para no llevar a cabo el trabajo
*** Compromiso
**** Disponibilidad total
***** No hay que trabajar de 9 a 11 pero hay que estar dispuesto a hacerlo \nPuede no encajar con algunas formas de entender la vida
**** Nuevos retos
***** Hay que actuar por encima de los propios conocimientos \nLa variedad y los retos permiten evolucionar mas rapido \nPuede ser estresante pero nunca aburrido
**** Viajes
***** Suele demandar movilidad geografica
** ¿Cómo es la carrera profesional en consultoria?
*** Modelo de carrera 
**** Modelo RRHH 
***** Modelo de categorias \nPlan de carrera \n Proceso de evolcion \nPlan de formacion
****** Flexible \nAjustado a la necesidad del mercado \nValido para diferentes tipos de servicio
*** Puestos
**** Junior \nSenior \nGerente \nDirector
@endmindmap
```
## Consultoria de Software
```plantuml
@startmindmap
title Consultoría de Software
* Consultoría de Software
** AXPE consulting
*** Es una empresa de consultoria española, presta servicios a 30 de las empresas mas grandes
** Desarollo del software
*** Proceso
**** Es un proceso muy largo que comienza cuando un cliente tiene \nuna necesidad en su negocio para ser mas productivo
***** 1.- Consultoria
****** Hablar con el cliente y ver que requerimientos tiene
***** 2.- Estudio de viabilidad
****** Analizas lo que quiere, ver ventajas, ver coste del desarollo
***** 3.- Diseño funcional
****** Ver que informacio necesita el sistema de entrada y que informacion de salida te tiene que dar \nPrototipo del software
***** Componente Sectorial /nGente que conozca sobre el tema del que va a ser el software
***** 4.- Diseño Tecnico
****** Diseñador tecnico traslada una necesidad a nivel tecnico con un lenguaje de \nprogramacion en un entorno de hardware, ver la base de datos que se necesita etc.
***** 5.-Prueba 
****** Ver que todo funciona y ver que todos los requerimientos solicitados se cumplen
***** 6.- Implantar el programa
****** En todos los lugares donde se necesiten
*** Relaciones con personas
**** Jefe del proyecto se encarga de planificar las fases del proyecto, ver que recursos se necesitan, \nhablar con el cliente y dotar de los recursos humanos necesarios para cumplir en tiempo y forma
*** ¿Como se materializan los procesos? 
**** El clinte puede pedir que se trbaje en sus instalaziones 
**** Tambien se desarolla en las propias istalaciones
***** Se industrializa en fatorias de sofwtare
** Actividades desarrolladas en AXPE consulting 
*** Empresa de consultoria e informatica 
**** Realizan todas las tareas que permiten crear o mantener la informatica de una empresa \nDar servicio a empresas particulares
**** ¿Que necesita una empresa en el plan iformatico para empezar o mantenerse?
**** Se crea la infrestructura \nInternet, linea de comunicaciones \nSeguridad de la informacion \nAplicaciones
*** Hacer que siga funcionando y si deja de funcionar, hacerla volver a funcionar
**** Dependiendo del tamaño de una empresa \nEn una empresa pequeña incluso ellos mismos con una serie de mecanismos pueden hacerlo solos \nEn una empresa grande se debe estar siempre para ver que todo funcione
*** Como influye el factor de escala
**** Se adecua a la necesidas del servicio, proceso, cliente
***** Oficionas de proyecto para un gran cliente, pues tiene muchos proyectos, \nNo solo se conoce la informatica, se tiene que conocer el sector del cliente
***** Clientes pequeños es mas personalizada, equipos pequeños 
*** Funcionmiento critico
**** Depende del negocio su propio nivel de criticidad
**** Se debe tener una alta seguridad por la sensibilidad de datos
** Tendencias en el desarollo de servicio
*** Las tendencias actuales empiezan por cambiar el concepto de lo que antes era la infrestructura de una empresa
**** Ahora se almacena la informacion y se trabaja en la nube, en donde ellos realizan la seguridad 
*** Cambio en la forma de tratar la informacion
**** Base de datos relacionales a Base de datos no relacionales
*** Intentar facilitar la vida
*** BIGDATA
**** Grandes volumenes de datos
*** Metodologias agiles
**** Permiten tener proyectos mas pequeños mucho mas rapido
*** Inconvenientes
**** Los negocios que tiene todo en la nube no tiene inconveientes, \npero los que no quieren tener su informacion en manos de terceros tienen problemas y es complicado convencerlos
**** Se rigen por diferentes leyes y puede generar problemas \nPuede haber problemas de seguridad
@endmindmap
```
## Aplicacion de la Ingenieria se Software
```plantuml
@startmindmap
title Aplicacion de la Ingenieria de Software
* Aplicacion de la Ingenieria se Software 
** Stratesys 
*** Es una empresa de servicios de tecnologia 
*** SAP
**** Las empresas tienden a productos comerciales y soluciones RP
**** Es un paquete de software configurable
**** Se desarrollan aplicaciones en JAVA para solucionar problemas
**** Se instala una base de datos
**** Se necesitan 7 u 8 personas
*** Metodologia
**** Analisis
***** Es el 20% del trabajo
***** Auditada internamente y externamente
***** Se recoge los requisitos que necesita el clinte y en que area
***** Se necesita tener una vision global de lo que hace la empresa
***** Se ve cada uno de los procesos que realiza la empresa
***** Documento de requisito funcional donde detallan todo lo que hace la empresa
***** Docuemnto de analisis funcional, de todo lo que se necesita como se va a hacer
***** Plan de proyecto, que se va a realizar, quien lo va a realizar y el epacio temporal
***** El clinte debe aprovar el diseño funcional
***** Analisis tecnico en el cual se ve todo lo que se va a hacer, como se va a hacer a nivel tecnico
**** Desarollo
***** Es el 60% del trabajo
***** Prototipado en donde se da una primera version
***** Gestion del alcance o cambios de alcance, ampliaciones de alncace
***** Fase de pruebas 
***** Se divide en 2 partes Consultores funcionales y Programacion
***** Se va documentanto todo el programa
**** Fase de Pruebas
***** Es el ultimo 20% del proyecto
***** Se documentan estas pruebas
@endmindmap
```